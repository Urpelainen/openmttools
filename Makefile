CC=gcc
CFLAGS=-I.
LIBS = -lpthread
BINDIR=bin
OBJDIR=obj

# executable names
LOADER=mtinit
LAUNCHER=mtdaemon

all: dirs $(LOADER) $(LAUNCHER)

.PHONY: dirs clean

dirs: $(BINDIR) $(OBJDIR)
$(BINDIR): 
	mkdir -p $(BINDIR)
$(OBJDIR): 
	mkdir -p $(OBJDIR)

clean:
	rm -f $(BINDIR)/$(LOADER) $(BINDIR)/$(LAUNCHER) $(OBJDIR)/*.o

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

$(LOADER): $(OBJDIR)/$(LOADER).o
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^

$(LAUNCHER): $(OBJDIR)/$(LAUNCHER).o
	$(CC) $(CFLAGS) $(LIBS) -o $(BINDIR)/$@ $^
